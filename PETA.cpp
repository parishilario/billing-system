#include <iostream>
#include <cstdlib>
#include <conio.h>
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;
int total = 0, qty;
double price = 0;

string accept_user_input() {
	string input;
	int code = getche(); // scan individual character as ascii
	while (code != 13) { // terminate loop if user pressed enter key
		if (code != 8) { // accept character if not backspace
			input.push_back(code);
		} else if (input.size() != 0) { // if user entered backspace
			input.erase(input.size()-1);
			cout << " \b";
		}
		code = getche();
	}
	
	return input;
}

// check if string is a number
bool is_number(const std::string& s) {
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

int accept_qty() {
	string qty = accept_user_input();
	int q = 0;
	if (is_number(qty)) {
		// convert string qty to int qty
		stringstream ss_qty(qty);
		ss_qty >> q;
		if (q <= 0) return -9999; // if negative number
	} else {
		return -9999; // return -9999 if not a number
	}
    
	return q;
}

void qty_error_handling(string &item_desc, int &qty) {
	while (qty == -9999) {
		cout << item_desc << "Invalid quantity! Try again? y/n ";
		int answer = getche(); // scan if y/n
		if (answer != 121 && answer != 89) std::exit(0);
		else if (answer == 121 || answer == 89) {
			// Delete entire line if user wants to try again.
			for (int i=0; i<34; i++) cout << "\b";
			for (int i=0; i<34; i++) cout << " ";
			for (int i=0; i<34; i++) cout << "\b";
		}
		qty = accept_qty();
	}
}

int main() {
	int total = 0, cash, change;
	char selection;
	cout << "\n";
	cout << "\t\t\t|Borgers ni Paris|" << endl;
	cout << "\n";
	cout << "\t  Codes\t      Item Description\t        Price\n";
	cout << "\t    1\t     Tuna Sandwich w/Hot Tea\t        105.00\n";
	cout << "\t    2\t     Aloha Burger w/Pineapple Shake\tP105.00\n";
	cout << "\t    3\t     Saucy Burger\t                P100.00\n";
	cout << "\t    4\t     Healthy Burger\t                P90.00\n";
	cout << "\t    5\t     Spicy Chicken Burger\t        P85.00\n";
	cout << "\t    6\t     Peanut Butter n' Jelly Sandwich\tP85.00\n";
	cout << "\t    7\t     Pineapple Shake\t                P80.00\n";
	cout << "\t    8\t     Strawberry Shake\t                P70.00\n";
	cout << "\t    9\t     Cheese Burger\t                P65.00\n";
	cout << "\t    10\t     Cheese Sandwich\t                P65.00\n";
	cout << "\t    11\t     Classic Burger\t                P60.00\n";
	cout << "\t    12\t     Classic Sandwich\t                P60.00\n";
	cout << "\t    13\t     Avocado Shake\t                P60.00\n";
	cout << "\t    14\t     Banana Shake\t                P60.00\n";
	cout << "\t    15\t     Mango Shake\t                P60.00\n";
	cout << "\t    16\t     Hot Tea\t                        P50.00\n";
	cout << "\t    17\t     Iced Tea\t                        P40.00\n";
	cout << "\t    18\t     Coffee\t                        P30.00\n";
	cout << "\t    19\t     Bottled Water\t                P20.00\n\n";
	cout << "\tEnter item code (press 0 to quit):" << endl;
	cout << "\t|======|==================================|=============|===============|" << endl;
	cout << "\t| Code | Item Description                 |  Qty\t|  Amount\t|" << endl;
	cout << "\t|======|==================================|=============|===============|" << endl;
	
	string code;
	
	do {
		cout << "\t|  ";
		code = accept_user_input();
    	if (code.compare("1") == 0) {
    		string item_desc = "\t|  1   | Tuna Sandwich w/Hot Tea          |  ";
    		price = 105;
    		cout << item_desc;
    		qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("2") == 0) {
			string item_desc = "\t|  2   | Aloha Burger w/Pineapple Shake   |  ";
			double price = 105;
    		cout << item_desc;
    		int qty = accept_qty();
    		
    		qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("3") == 0) {
			string item_desc = "\t|  3   | Saucy Burger                     |  ";
			double price = 100;
    		cout << item_desc;
    		int qty = accept_qty();
    		
    		qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("4") == 0) {
			string item_desc = "\t|  4   | Healthy Burger                   |  ";
			double price = 90;
    		cout << item_desc;
    		int qty = accept_qty();
    		
    		qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("5") == 0) {
			string item_desc = "\t|  5   | Spicy Chicken Burger             |  ";
			double price = 85;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("6") == 0) {
			string item_desc = "\t|  6   | Peanut Butter n' Jelly Sandwich  |  ";
			double price = 85;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("7") == 0) {
			string item_desc = "\t|  7   | Pineapple Shake                  |  ";
			double price = 80;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("8") == 0) {
			string item_desc = "\t|  8   | Strawberry Shake                 |  ";
			double price = 70;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("9") == 0) {
			string item_desc = "\t|  9   | Cheese Burger                    |  ";
			double price = 65;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("10") == 0) {
			string item_desc = "\t|  10  | Cheese Sandwich                  |  ";
			double price = 65;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("11") == 0) {
			string item_desc = "\t|  11  | Classic Burger                   |  ";
			double price = 60;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("12") == 0) {
			string item_desc = "\t|  12  | Classic Sandwich                 |  ";
			double price = 60;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("13") == 0) {
			string item_desc = "\t|  13  | Avocado Shake                    |  ";
			double price = 60;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("14") == 0) {
			string item_desc = "\t|  11  | Banana Shake                     |  ";
			double price = 60;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("15") == 0) {
			string item_desc = "\t|  15  | Mango Shake                      |  ";
			double price = 60;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("16") == 0) {
			string item_desc = "\t|  16  | Hot Tea                          |  ";
			double price = 50;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("17") == 0) {
			string item_desc = "\t|  17  | Iced Tea                         |  ";
			double price = 40;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("18") == 0) {
			string item_desc = "\t|  18  | Coffee                           |  ";
			double price = 30;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("19") == 0) {
			string item_desc = "\t|  19  | Bottled Water                    |  ";
			double price = 20;
    		cout << item_desc;
    		int qty = accept_qty();
    		
			qty_error_handling(item_desc, qty);
			
			double subtotal = qty * price;
			cout << item_desc << qty << "\t\t|  " << subtotal << ".00\t|" << endl;
    		total += subtotal;
		} else if (code.compare("0") == 0) {
			cout << "\t|  0   | N/A                              |  0\t\t|    0.00\t|";
		} else {
			cout << "\t| " << " " << code << " - Invalid item code! Try again? y/n   ";
			int answer = getche(); // scan if y/n
			if (answer != 121 && answer != 89) std::exit(0);
			else if (answer == 121 || answer == 89) {
				// Delete entire line if user wants to try again.
				for (int i=0; i<code.size() + 50; i++) cout << "\b";
				for (int i=0; i<code.size() + 50; i++) cout << " ";
				for (int i=0; i<code.size() + 50; i++) cout << "\b";
			}
		}
	} while(code.compare("0") != 0);
	
	cout << endl;
	cout << "\t|======|==================================|=============|===============|" << endl;
	cout << "\t                                                  Total:    " << total << endl;
	do {
		cout << "\t                                          Cash Tendered:    ";
		cash = accept_qty();
		if (cash == -9999) {
		cout << "\t                                          Cash Tendered:    Invalid input. try again? y/n - ";
		int answer = getche(); // scan if y/n
		if (answer != 121 && answer != 89) std::exit(0);
		else if (answer == 121 || answer == 89) {
			// Delete entire line if user wants to try again.
				for (int i=0; i<100; i++) cout << "\b";
				for (int i=0; i<100; i++) cout << " ";
				for (int i=0; i<100; i++) cout << "\b";
		}
	} else if (cash < total) {
		cout << "\t                                          Cash Tendered:    Not enough balance. try again? y/n - ";
		int answer = getche(); // scan if y/n
		if (answer != 121 && answer != 89) std::exit(0);
		else if (answer == 121 || answer == 89) {
			// Delete entire line if user wants to try again.
				for (int i=0; i<100; i++) cout << "\b";
				for (int i=0; i<100; i++) cout << " ";
				for (int i=0; i<100; i++) cout << "\b";
		}
	} else {
		cout << endl;
	}
  } while (cash == -9999 || cash < total);
  
  change = cash - total;
  cout << "\t                                                 Change:    " << change;
  cout << "\n\t | Thank you for choosing Borgers ni Paris :)";
	return 0;
}

//
//======|==========================|=========|
// Code | Item Description         | Amount  |
//======|==========================|=========|
//  1   | Tuna Sandwich w/Hot Tea  |  105.00 |
//  5   | Spicy Chicken Burger     |   85.00 |
//============================================
//							Total:  190.00
//					Cash Tendered:  1000.00
//					       Change:  810.00
